""" Demo URLs module
"""
from django.urls import path
from django.contrib import admin

from django.conf import settings
from django.conf.urls.static import static
from tests.app.views import ExampleView, ExampleGridView


urlpatterns = [
    path("", ExampleView.as_view(), name="home"),
    path("grid/", ExampleGridView.as_view(), name="grid"),
    path("admin/", admin.site.urls),
]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
