""" Django Apricot Core views module
"""
from dacommon.views import BaseView


class ExampleView(BaseView):
    """View for example"""

    template_name = "dapricot/example.html"


class ExampleGridView(BaseView):
    """View for grid example"""

    template_name = "dapricot/example_grid.html"
