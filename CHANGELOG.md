# Change Log

## [2023.0.8] - 2023-07-08

### Fixed
- changed `datetime.date` to `datetime.now` in `get_period` default

## [2023.0.7] - 2023-06-28

### Fixed
- Converter base class operations

## [2023.0.6] - 2023-06-27

### Fixed
- integration request trying cache in app mode

## [2023.0.5] - 2023-06-26

### Fixed
- failed expired cache validation

## [2023.0.4] - 2023-06-22

### Changed
- minor changes for compatibility with `damusic`

## [2023.0.3] - 2023-06-22

### Fixed
- `BaseAppView` auth flow failing

## [2023.0.2] - 2023-06-19

### Added
- `PeriodName` Enum and `get_period` method for easy control on datetime objects
- base grid style
- added `set_as_app` to `BaseIntegration` for anonymous user compatibility
- `BaseAuthView` for integrations
- `BaseAppView` for anonymous user compatibility with integrations

### Changed
- `BaseIntegration` moved to `integration.bases` module
- `IntegrationDataOrigin` and `CachedRequest` moved to `integration.models` module

### Fixed
- missing `query` un unique_together rule for `IntegrationRequestCache`
- minor forms style changes

## [2023.0.1] - 2023-06-12

### Added
- cache on `GoogleIntegration.user`
- added query data to `IntegrationRequestCache`
- `get_period_name` template filter to retrieve period names

### Changed
- splitted utils to `utils.converters`, `utils.datetime` and `utils.generators`

## [2023.0.0] - 2023-06-10

### Added
- common utils
- bases for API integrations
- base Google integration
- base html templates
- base scss templates and compiler