# Django Apricot Common : v2023.0.8

Common tools for django apricot suite

[Documentation](https://softapricot.gitlab.io/django-apricot-common)

## Development

To prepare develoment environment use poetry

```console
$ pip install poetry==1.4.2
$ poetry install
$ poetry self add poetry-bumpversion
```

To update version across all references use the following command:

```console
$ poetry version <bump rule>
```

### Available bump rules:

* patch
* minor
* major
* prepatch
* preminor
* premajor
* prerelease

## Running tests and validations

```console
$ poetry run tox -e <testenv name>
```

### Available testenvs:

* pylint
* flake8
* build
* coverage
