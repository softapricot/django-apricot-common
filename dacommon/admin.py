""" Admin module
"""
from django.contrib import admin
from dacommon.models import IntegrationRequestCache, IntegrationToken
from dacommon.utils.datetime import get_datetime
from dacommon.integrations.models import IntegrationDataOrigin


class OriginListFilter(admin.SimpleListFilter):
    """Filter to show origin name instead of id"""

    title = "Integration's data origin"

    parameter_name = "origin"

    def lookups(self, request, model_admin):
        return IntegrationDataOrigin.choices

    def queryset(self, request, queryset):
        return (
            queryset if self.value() is None else queryset.filter(origin=self.value())
        )


def expiration_date(*args):
    """method to show timestamp humanly readable"""
    date = get_datetime(args[1].expires_at)
    return date


def data_origin(*args):
    """Filter to show origin name instead of id"""
    items = {item[0]: item[1] for item in IntegrationDataOrigin.choices}

    return items[int(args[1].origin)]


@admin.register(IntegrationRequestCache)
class IntegrationRequestCacheAdmin(admin.ModelAdmin):
    """Model admin for Request's cache"""

    fields = [("origin", "user"), ("endpoint", "expires_at"), "query", "response"]
    list_display = ["endpoint", "data_origin", "expiration_date"]
    list_filter = ["user", OriginListFilter]
    readonly_fields = ["expires_at", "origin", "endpoint"]

    data_origin = data_origin
    expiration_date = expiration_date


@admin.register(IntegrationToken)
class IntegrationTokenAdmin(admin.ModelAdmin):
    """Model admin for tokens"""

    fields = [("origin", "user"), ("name", "expires_at"), "token"]
    list_display = ["name", "data_origin", "expiration_date"]
    list_filter = ["user", OriginListFilter]
    readonly_fields = ["token", "expires_at", "origin"]

    data_origin = data_origin
    expiration_date = expiration_date
