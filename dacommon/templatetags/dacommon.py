""" Django Apricot accounts tags module
"""
from django import template
from django.db.models import Q
from django.contrib.auth import get_user_model
from dacommon.utils.datetime import PeriodName


register = template.Library()


@register.filter(name="get_user")
def get_user(value):
    """Returns user object if exists"""
    model = get_user_model()
    user = model.objects.filter(Q(username=value) | Q(email=value))
    if user.count() > 0:
        return user.first()

    return None


@register.filter(name="get_period_name")
def get_period_name(value):
    """Returns period name object if exists"""
    period = PeriodName(value)

    return period
