""" Django Apricot Core views module
"""
from enum import Enum

from django.views.generic.base import View
from django.shortcuts import render
from django.conf import settings

__all__ = ["StyleMixin", "DjangoStyleOverrideMixin", "BaseView", "MessageView"]


class StyleMixin:
    """Style mixin for dapricot pages"""

    template_name = None
    template_variables = {
        "style_sources": getattr(settings, "STYLESHEETS_SOURCES", []),
        "script_sources": getattr(settings, "SCRIPTS_SOURCES", []),
        "metas": [
            ("viewport", "width=device-width, initial-scale=1"),
        ],
        "external_scripts": [
            "https://code.jquery.com/jquery-3.6.0.min.js",
        ],
        "external_styles": [],
    }

    def add_style_sources(self, style_list: list, is_external=False):
        """Adds style source to the context"""
        for uri in style_list:
            if is_external:
                self.template_variables["external_styles"].append(uri)
            else:
                self.template_variables["style_sources"].append(uri)

    def add_script_sources(self, script_list: list, is_external=False):
        """Adds script source to the context"""
        for uri in script_list:
            if is_external:
                self.template_variables["external_scripts"].append(uri)
            else:
                self.template_variables["script_sources"].append(uri)


class DjangoStyleOverrideMixin(StyleMixin):
    """Style mixin for django pages"""

    def get(self, request, *args, **kwargs):
        """Handle GET requests"""
        self.extra_context = self.template_variables
        self.extra_context.pop(
            "form", None
        )  # fixes bug of persistent form object between views
        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        """Handle POST requests"""
        self.extra_context = self.template_variables
        return super().post(request, *args, **kwargs)


class BaseView(StyleMixin, View):
    """Base view for custom template"""

    def setup(self, request, *args, **kwargs):
        self.add_data(
            "installed_apps",
            [app for app in settings.INSTALLED_APPS if app.startswith("da")],
        )

        super().setup(self, request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        """Get method"""
        return render(
            request, self.template_name, self.template_variables, *args, **kwargs
        )

    def add_data(self, key, value, append=False):
        """Adds data to the context"""
        if append:
            if key not in self.template_variables:
                data = []
            else:
                data = self.template_variables[key]

            data.append(value)

        else:
            data = value

        self.template_variables[key] = data


class MessageView(BaseView):
    """Signup view"""

    template_name = "dapricot/message.html"
    message_title = ""
    message = ""

    class MessageType(Enum):
        """Message type style"""

        INFO = 0
        WARNING = 1
        ERROR = 2

    def get(self, request, *args, **kwargs):
        """Get request"""
        if isinstance(self.message, str):
            self.message = [
                self.message,
            ]
        self.add_data("message", self.message)
        self.add_data("message_title", self.message_title)

        return super().get(request, *args, **kwargs)
