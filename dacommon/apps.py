""" Django Apricot app module
"""
from django.core import checks
from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _

from dacommon.checks import check_dependencies, AppDependencies
from dacommon import app_settings
from dacommon.utils import setup_default_settings


def check_accounts_dependencies(**_):
    """Checks dependencies"""
    dependencies = AppDependencies()
    dependencies.add_dependency("compressor", 401)

    return check_dependencies(
        DjangoApricotCommonConfig.name,
        DjangoApricotCommonConfig.label,
        dependencies.deps,
    )


class DjangoApricotCommonConfig(AppConfig):
    """App's config"""

    default_auto_field = "django.db.models.AutoField"
    name = "dacommon"
    label = "dacommon"
    verbose_name = _("Django Apricot Commons")

    def ready(self):
        checks.register(check_accounts_dependencies, self.name)
        setup_default_settings(app_settings)
