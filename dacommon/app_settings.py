""" Default settings module
"""
from dacommon.utils import get_project_scripts, get_stylesheets_sources

STYLESHEETS_SOURCES = get_stylesheets_sources()
SCRIPTS_SOURCES = get_project_scripts()

COMPRESS_OFFLINE_CONTEXT = {
    "stylesheets_sources": STYLESHEETS_SOURCES,
    "scripts_sources": SCRIPTS_SOURCES,
}
