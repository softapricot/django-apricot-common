""" Dapricot Core utils module
"""
from django.conf import settings


def setup_default_settings(app_settings):
    """App's default settings setup"""
    for name in dir(app_settings):
        if name.isupper():
            if not hasattr(settings, name):
                setattr(settings, name, getattr(app_settings, name))


def get_project_scripts(custom_base=None) -> list:
    """Gets dapricot & addons scripts"""
    scripts = []
    if custom_base is not None:
        scripts.append(custom_base)

    return scripts


def get_stylesheets_sources(custom_base=None) -> list:
    """Gets dapricot & addons stylesheets"""
    stylesheets = [
        "dapricot/scss/base.scss" if custom_base is None else custom_base,
    ]
    if "damusic" in settings.INSTALLED_APPS:
        stylesheets.append("dapricot/scss/music.scss")
    if "dafitness" in settings.INSTALLED_APPS:
        stylesheets.append("dapricot/scss/fitness.scss")
    if "dapricot.photos" in settings.INSTALLED_APPS:
        stylesheets.append("dapricot/scss/photos.scss")

    return stylesheets
