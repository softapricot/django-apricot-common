""" Dapricot Core utils module
"""
from enum import Enum
from datetime import datetime, timedelta
from django.conf import settings


class PeriodName(Enum):
    """Integrations periods"""

    DAY = "day"
    WEEK = "week"
    MONTH = "month"
    YEAR = "year"


def get_datetime(value, datetime_format: str = settings.DATETIME_FORMAT):
    """Method to parse datetime using integration's specific format"""
    if isinstance(value, (int, float)):
        value = datetime.fromtimestamp(value)
    elif isinstance(value, str):
        value = datetime.strptime(value, datetime_format)
    elif value is None:
        value = datetime.now()

    return value


def is_datetime_greater_than(
    first_date, second_date, datetime_format: str = settings.DATETIME_FORMAT
):
    """Validates if first date is greater than second date"""
    first_date = get_datetime(first_date, datetime_format=datetime_format)
    second_date = get_datetime(second_date, datetime_format=datetime_format)

    return (first_date - second_date).total_seconds() > 0


def is_datetime_past(date):
    """Validates if date is in the past"""
    return is_datetime_greater_than(datetime.now(), date)


def get_period(
    period_unit: PeriodName = PeriodName.DAY,
    period_size: int = 1,
    reference_date=datetime.now(),
):
    """return date period"""
    reference_date = get_datetime(reference_date).replace(hour=0, minute=0, second=0)
    start_period = reference_date
    if period_unit == PeriodName.DAY:
        end_period = reference_date + timedelta(days=period_size)
    if period_unit == PeriodName.WEEK:
        start_period = reference_date - timedelta(days=reference_date.weekday())
        reference_date += timedelta(days=7 * period_size)
        end_period = reference_date + timedelta(days=7 - reference_date.weekday())
    if period_unit == PeriodName.MONTH:
        start_period = reference_date.replace(day=1)
        reference_date = start_period + timedelta(months=period_size + 1)
        end_period = reference_date - timedelta(days=1)
    if period_unit == PeriodName.YEAR:
        start_period = reference_date.replace(day=1, month=1)
        reference_date = start_period + timedelta(years=period_size + 1)
        end_period = reference_date - timedelta(days=1)

    return start_period, end_period - timedelta(seconds=1)
