""" Generators module """
import string
import random


def get_random_string(length, letters=True, digits=True, punctuation=True):
    """Returns a random string with specified length"""
    characters = ""

    if letters:
        characters += string.ascii_letters
    if digits:
        characters += string.digits
    if punctuation:
        characters += string.punctuation

    result_str = "".join(random.choice(characters) for _ in range(length))

    return result_str
