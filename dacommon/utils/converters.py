""" Converters module
"""
from enum import Enum


class BaseConverter:
    """Base unit converter class"""

    inverse_convertion = False

    def __init__(self, value, unit):
        self._initial_value = value
        self._initial_unit = unit

    @property
    def value_on_smallest_unit(self):
        """Return value in the smallest unit"""
        return self._initial_value * self._initial_unit.value

    def _convert_value_to_unit(self, unit):
        value = self.value_on_smallest_unit
        if self.inverse_convertion:
            value *= unit.value
        else:
            value /= unit.value

        return value

    def ordered_units(self):
        """Returns units list order desc"""

        def take_second(elem):
            """returns second element of a tuple"""
            return elem[1]

        units = [(item.name, item.value) for item in getattr(self, "Unit", None)]
        units.sort(key=take_second, reverse=True)

        return units

    def to_string(self, separator=" "):
        """returns the value in a string with all units matches"""
        value = self.value_on_smallest_unit
        value_string = []

        for unit_name, unit_value in self.ordered_units():
            quotient, value = divmod(value, unit_value)
            value_string.append(
                f"{quotient}{unit_name.lower().replace('_','/')}{'s' if quotient>1 else ''}"
            )
            if value <= 0:
                break

        return separator.join(value_string)


class DistanceConverter(BaseConverter):
    """Distance converter class"""

    class Unit(Enum):
        """distance units"""

        MT = 1
        KM = 1000

    def get_meters(self):
        """returns value in meters"""
        return self._convert_value_to_unit(self.Unit.MT)

    def get_kilometers(self):
        """returns value in kilometers"""
        return self._convert_value_to_unit(self.Unit.KM)


class TimeConverter(BaseConverter):
    """Time converter class"""

    class Unit(Enum):
        """time units"""

        S = 1
        MIN = 60
        HR = 3600

    def get_seconds(self):
        """returns value in seconds"""
        return self._convert_value_to_unit(self.Unit.S)

    def get_minutes(self):
        """returns value in minutes"""
        return self._convert_value_to_unit(self.Unit.MIN)

    def get_hours(self):
        """returns value in hours"""
        return self._convert_value_to_unit(self.Unit.HR)


class SpeedConverter(BaseConverter):
    """Speed converter class"""

    inverse_convertion = True

    class Unit(Enum):
        """speed units"""

        MTS_S = 1
        KMS_HR = 3.6

    def get_mts_per_second(self):
        """returns value in mts/s"""
        return self._convert_value_to_unit(self.Unit.MTS_S)

    def get_kms_per_hr(self):
        """returns value in kms/hr"""
        return self._convert_value_to_unit(self.Unit.KMS_HR)
