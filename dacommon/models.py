""" Models module
"""
from datetime import datetime, timedelta
from django.db import models
from django.contrib.auth import get_user_model
from dacommon.utils.datetime import get_datetime, is_datetime_past
from dacommon.integrations.models import IntegrationDataOrigin


class IntegrationRequestCache(models.Model):
    """Model to store tokens for integration with remote services"""

    endpoint = models.URLField()
    response = models.TextField()
    query = models.TextField(default="")
    user = models.ForeignKey(
        get_user_model(), on_delete=models.CASCADE, related_name="caches"
    )
    expires_at = models.FloatField(default=0)
    origin = models.CharField(max_length=1)

    class Meta:
        verbose_name = "cache"
        verbose_name_plural = "caches"
        unique_together = ("endpoint", "user", "query")

    def __str__(self):
        return self.endpoint

    @property
    def is_expired(self):
        """Validates if cache is past expiration date"""
        date = get_datetime(self.expires_at)
        return is_datetime_past(date)


class IntegrationToken(models.Model):
    """Model to store tokens for integration with remote services"""

    name = models.CharField(max_length=20)
    user = models.ForeignKey(
        get_user_model(), on_delete=models.CASCADE, related_name="tokens"
    )
    token = models.CharField(max_length=400)
    expires_at = models.IntegerField(default=0)
    origin = models.CharField(max_length=1)

    class Meta:
        verbose_name = "token"
        verbose_name_plural = "tokens"
        unique_together = (
            "name",
            "user",
            "origin",
        )

    def __str__(self):
        return self.name

    @property
    def is_expired(self):
        """Validates if token is past expiration date"""
        date = get_datetime(self.expires_at)
        return is_datetime_past(date)


class ProfileModelMixin:
    """Profile model mixin with"""

    def get_token(self, name) -> IntegrationToken:
        """Looks for named user's token"""
        token = None
        for item in self.get_token_list():
            if item.name == name:
                token = item
                break

        return token

    def get_token_list(self) -> list:
        """Returns user's token list"""
        tokens = list(self.user.tokens.filter(origin=self.origin))
        return tokens

    def create_or_update_token(self, name, value, expires_at=None):
        """Saves integration token data"""
        token = self.get_token(name)

        if expires_at is None:
            expires_at = datetime.now()
            expires_at = expires_at.timestamp()

        if token is None:
            token = IntegrationToken.objects.create(
                name=name,
                user=self.user,
                token=value,
                expires_at=expires_at,
                origin=self.origin,
            )
        else:
            token.token = value
            token.expires_at = expires_at
            token.save()

    def get_cache_list(self) -> list:
        """Returns user's cache list"""
        caches = list(self.user.caches.filter(origin=self.origin))
        return caches

    def get_cache(self, endpoint, query) -> IntegrationRequestCache:
        """Looks for named user's cache"""
        cache = self.user.caches.filter(
            origin=self.origin, endpoint=endpoint, query=query
        )
        if cache.count() > 0:
            return cache.last()

        return None

    def create_or_update_cache(self, endpoint, response, query, expires_at=None):
        """Saves integration cache data"""
        cache = self.get_cache(endpoint, query)

        if expires_at is None:
            expires_at = datetime.now() + timedelta(days=1)
            expires_at = expires_at.timestamp()

        if cache is None:
            cache = IntegrationRequestCache.objects.create(
                endpoint=endpoint,
                user=self.user,
                response=response,
                origin=self.origin,
                expires_at=expires_at,
                query=query,
            )
        else:
            cache.response = response
            cache.expires_at = expires_at
            cache.save()


class GoogleProfile(models.Model, ProfileModelMixin):
    """Fitness Profile model"""

    user = models.OneToOneField(get_user_model(), on_delete=models.CASCADE)
    origin = models.CharField(
        choices=IntegrationDataOrigin.choices,
        max_length=1,
        default=IntegrationDataOrigin.GOOGLE,
    )

    class Meta:
        verbose_name = "google profile"
        verbose_name_plural = "google profiles"
        unique_together = (
            "origin",
            "user",
        )

    def __str__(self):
        return f"{self.user}_{self.origin}"
