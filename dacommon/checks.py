""" Django Apricot checks module
"""
from django.apps import apps
from django.core import checks


class AppDependencies:
    """Apps dependencies object"""

    _dependencies = []

    def add_dependency(self, app_name, code):
        """Adds dependency to the object"""
        self._dependencies.append((app_name, code))

    @property
    def deps(self):
        """Returns deopendencies list"""
        return self._dependencies


def check_dependencies(app_name, app_label, app_dependencies: AppDependencies):
    """Dependencies validation for accounts app"""
    if not apps.is_installed("dacommon"):
        return []
    errors = []

    for dep_name, error_code in app_dependencies:
        if not apps.is_installed(app_name):
            errors.append(
                checks.Error(
                    f"'{dep_name}' must be in INSTALLED_APPS in order to use {app_name} application.",
                    id=f"{app_label}.E{error_code}",
                )
            )

    return errors
