""" Strava integration module
"""
from enum import Enum
from urllib.parse import urlencode
from datetime import datetime, timedelta

from django.shortcuts import redirect
from django.conf import settings
from django.urls.base import reverse

from dacommon.integrations.bases import BaseIntegration
from dacommon.utils.generators import get_random_string
from dacommon.models import GoogleProfile
from dacommon.integrations.models import IntegrationDataOrigin


class Scopes(Enum):
    """Authorization scopes"""

    USERINFO_EMAIL = "https://www.googleapis.com/auth/userinfo.email"
    USERINFO_PROFILE = "https://www.googleapis.com/auth/userinfo.profile"


class GoogleIntegration(BaseIntegration):
    """Integration class for Strava API"""

    origin = IntegrationDataOrigin.GOOGLE
    base_uri = "https://www.googleapis.com/oauth2/v1/"
    datetime_format = "%Y-%m-%dT%H:%M:%SZ"
    profile_model = GoogleProfile

    def __init__(self, context, scopes: list, user=None):
        self._client_id = settings.GOOGLE_CLIENT_ID
        self._client_secret = settings.GOOGLE_CLIENT_SECRET
        self.scopes = " ".join(
            scope if isinstance(scope, str) else scope.value for scope in scopes
        )
        super().__init__(context, user)

    def authenticate(self, force=False):
        """Redirection object for integration authentication"""
        state = get_random_string(16, punctuation=False)

        self._context.session["photos_state"] = state
        data = {
            "response_type": "code",
            "client_id": self._client_id,
            "redirect_uri": f"http://{self._context.get_host()}{reverse('daphotos:auth')}",
            "state": state,
            "scope": self.scopes,
            "access_type": "offline",
            "include_granted_scopes": "true",
        }
        if force:
            data["prompt"] = "consent"

        url = "https://accounts.google.com/o/oauth2/v2/auth?" + urlencode(data)

        return redirect(url)

    def generate_auth_token(self, only_refresh: bool = False):
        """Generates authentication token"""
        payload = None
        refresh_token = self.profile.get_token("refresh_token")
        if refresh_token is not None and refresh_token.is_expired:
            payload = {
                "client_id": self._client_id,
                "client_secret": self._client_secret,
                "refresh_token": refresh_token.token,
                "grant_type": "refresh_token",
            }

        elif "code" in self._context.session and not only_refresh:
            payload = {
                "client_id": self._client_id,
                "client_secret": self._client_secret,
                "code": self._context.session["code"],
                "grant_type": "authorization_code",
                "redirect_uri": f"http://{self._context.get_host()}{reverse('daphotos:auth')}",
            }
            self._context.session.pop("code")

        if payload is not None:
            response = self.request(
                "https://oauth2.googleapis.com/token",
                GoogleIntegration.RequestAction.POST,
                payload=payload,
                verify=False,
            )
            tokens = response.json()

            expires_at = datetime.now() + timedelta(seconds=tokens["expires_in"])
            self.profile.create_or_update_token(
                "access_token", tokens["access_token"], expires_at.timestamp()
            )
            if "refresh_token" in tokens:
                self.profile.create_or_update_token(
                    "refresh_token", tokens["refresh_token"], expires_at.timestamp()
                )

    @property
    def user(self):
        """Returns user data"""
        response = self.request(
            "userinfo",
            GoogleIntegration.RequestAction.GET,
            auth_headers=self.auth_headers,
            use_cache=True,
        )
        return response.json()
