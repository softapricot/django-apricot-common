""" Data models module for integrations
"""

import json

from django.db import models


class IntegrationDataOrigin(models.IntegerChoices):
    """Integrations data origin"""

    STRAVA = 1
    SPOTIFY = 2
    GOOGLE = 3
    POLAR = 4


class CachedRequest:
    """Dummy request object to parse cached data"""

    def __init__(self, cache):
        self.cache = cache

    def json(self):
        """Returns cached data"""
        content = self.cache.response

        return json.loads(content)

    @property
    def status_code(self):
        """Returns dummy status code"""
        return 200
