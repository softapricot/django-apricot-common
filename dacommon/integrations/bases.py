""" Bases module for integrations
"""
import json
from enum import Enum
from abc import ABC, abstractmethod
from datetime import datetime, timedelta
import requests

from django.utils.http import urlencode
from django.utils.decorators import method_decorator
from django.contrib.auth.models import AnonymousUser
from django.conf import settings
from django.contrib.auth.decorators import login_required

from dacommon.views import BaseView
from dacommon.integrations.models import CachedRequest


class BaseIntegration(ABC):
    """Base integration class"""

    datetime_format = settings.DATETIME_FORMAT
    single_use = False

    @property
    @abstractmethod
    def profile_model(self):
        """Integration's profile model"""

    @property
    @abstractmethod
    def origin(self):
        """Integration's data origin"""

    @property
    @abstractmethod
    def base_uri(self):
        """Integration's data origin"""

    class RequestAction(Enum):
        """Request's actions"""

        GET = 0
        POST = 1

    def __init__(self, context, user=None):
        self._context = context
        self.set_profile(user)

    def set_as_app(self):
        """Changes class to single use mode"""
        self.single_use = True
        self._context.session["is_single_use"] = True

    @property
    def is_authenticated(self):
        """Validate if integration is already authenticated"""
        if isinstance(self.profile, AnonymousUser):
            return False

        token_list = self.profile.get_token_list()
        return len(token_list) > 0

    @abstractmethod
    def authenticate(self):
        """Authenticates API integration"""

    @property
    def auth_headers(self):
        """returns authentication headers for API usage"""
        self.generate_auth_token(True)

        headers = {"Authorization": "Bearer " + self.get_token("access_token").token}

        return headers

    @abstractmethod
    def generate_auth_token(self, *args):
        """Generates authentication token"""

    def get_token(self, name):
        """Gets token by name"""
        token = None
        if self.single_use:
            token = type(
                "Token", (), {"token": getattr(self, name, None), "is_expired": True}
            )
        else:
            token = self.profile.get_token(name)

        return token

    def get_token_list(self):
        """Gets token list"""
        tokens = list(self.profile.user.tokens.all())
        expired = [token for token in tokens if token.is_expired()]

        if len(tokens) == 0 or len(expired) > 0:
            self.generate_auth_token()
            tokens = list(self.profile.user.tokens.all())

        return tokens

    @property
    def profile(self):
        """Returns current integration's profile"""
        if self._profile is None:
            self.set_profile(self._context.user)
        return self._profile

    def set_profile(self, user):
        """Method to set profile for Integration instance"""
        if isinstance(user, AnonymousUser):
            self._profile = user

        else:
            profile = self.profile_model.objects.filter(origin=self.origin, user=user)

            if profile.count() == 0 and user != self._context.user:
                profile = None

            else:
                profile, _ = self.profile_model.objects.get_or_create(
                    origin=self.origin, user=user
                )

            self._profile = profile

    def request(
        self,
        endpoint,
        action: RequestAction,
        payload: dict = None,
        query: dict = None,
        auth_headers=None,
        timeout=5,
        use_cache=False,
        cache_expires_after=None,
        **kwargs,
    ):
        """API's request method"""
        endpoint = endpoint if "http" in endpoint else f"{self.base_uri}{endpoint}"
        uri = endpoint
        if query is not None and query != {}:
            uri += f"?{urlencode(query)}"

        cache = (
            self.profile.get_cache(endpoint, query)
            if use_cache and not self.single_use
            else None
        )

        if getattr(cache, "is_expired", True):
            auth_headers = {} if auth_headers is None else auth_headers
            auth_headers.update(kwargs.pop("headers", {}))

            if action == self.RequestAction.POST:
                request = requests.post(
                    uri, data=payload, headers=auth_headers, timeout=timeout, **kwargs
                )

            elif action == self.RequestAction.GET:
                request = requests.get(
                    uri, data=payload, headers=auth_headers, timeout=timeout, **kwargs
                )

            if use_cache and request.status_code == 200 and not self.single_use:
                data = json.dumps(request.json())
                expires_at = datetime.now() + (
                    timedelta(days=1)
                    if cache_expires_after is None
                    else cache_expires_after
                )
                self.profile.create_or_update_cache(
                    endpoint=endpoint,
                    response=data,
                    expires_at=expires_at.timestamp(),
                    query="{}" if query is None else query,
                )
        else:
            request = CachedRequest(cache)

        return request


class BaseAuthView(BaseView):
    """Base view for integration's api authentication"""

    integration = None

    @property
    @abstractmethod
    def integration_class(self):
        """Integration class"""

    @property
    @abstractmethod
    def integration_scopes(self):
        """Integration scopes"""

    def get(self, request, *args, force_auth=None, **kwargs):
        """Get method"""

        self.integration = self.integration_class(
            request, [scope.value for scope in self.integration_scopes]
        )

        if force_auth is not None or (
            not self.integration.is_authenticated and not request.GET.get("code", None)
        ):
            request.session["auth_redirect"] = "daaccounts:login"
            return self.integration.authenticate()

        return super().get(request, *args, **kwargs)

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)


class BaseAppView(BaseView):
    """Base view for single use integration app"""

    integration = None

    @property
    @abstractmethod
    def integration_class(self):
        """Integration class"""

    @property
    @abstractmethod
    def integration_scopes(self):
        """Integration scopes"""

    @abstractmethod
    def response_data(self, request):
        """Data retrieve and processing to send in response"""

    def get(self, request, *args, **kwargs):
        """Get method"""

        self.integration = self.integration_class(
            request, [scope.value for scope in self.integration_scopes]
        )
        self.integration.set_as_app()

        if request.session.get("code", None) is None and not request.session.get(
            "authenticated", False
        ):
            request.session["auth_redirect"] = request.build_absolute_uri()
            return self.integration.authenticate(True)

        try:
            self.integration.generate_auth_token()
            self.response_data(request)
        except TypeError:
            request.session["auth_redirect"] = request.build_absolute_uri().split("?")[
                0
            ]
            return self.integration.authenticate(True)

        return super().get(request, *args, **kwargs)
